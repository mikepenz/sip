package cmd

import (
	"fmt"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/markdown"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var IssuesCreate = cli.Command{
	Name:    "create",
	Aliases: []string{"new"},
	Usage:   "Create a new issue",
	Action:  doIssueCreate,
}

func doIssueCreate(_ *cli.Context) error {
	fmt.Println()
	url := color.New(color.FgYellow).Format(flag.FullURL())
	fmt.Println(color.New(color.FgCyan).Format("Creating a new issue for"), url)

	client, err := getClient(true)
	if err != nil {
		return err
	}

	var confirmed bool
	var title, body string

	for !confirmed {
		questions := []*survey.Question{
			{
				Name:     "title",
				Prompt:   &survey.Input{Message: "Title", Default: title},
				Validate: survey.Required,
			},
			{
				Name:   "body",
				Prompt: &survey.Multiline{Message: "Description", Default: body},
			},
		}
		answers := struct {
			Title string
			Body  string
		}{}

		if err := survey.Ask(questions, &answers); err != nil {
			return err
		}
		title = answers.Title
		body = answers.Body

		preview, err := markdown.Render(body)
		if err != nil {
			return err
		}

		fmt.Printf("%s\n\n%s\n", title, preview)
		confirm := &survey.Confirm{Message: "Preview above, enter to create or 'n' to edit", Default: true}

		if err := survey.AskOne(confirm, &confirmed); err != nil {
			return err
		}
	}

	issue, _, err := client.CreateIssue(flag.Owner, flag.Repo, gitea.CreateIssueOption{Title: title, Body: body})
	if err != nil {
		return err
	}

	info := color.Info
	cyan := color.New(color.FgCyan)
	fmt.Println(info.Format("Issue"), cyan.Format(fmt.Sprintf("#%d", issue.Index)), info.Format("created!"))
	fmt.Println(cyan.Format(fmt.Sprintf("%s/issues/%d", flag.FullURL(), issue.Index)))
	return nil
}
