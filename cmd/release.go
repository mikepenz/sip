package cmd

import (
	"fmt"
	"os"

	"go.jolheiser.com/sip/csv"
	"go.jolheiser.com/sip/markdown"
	"go.jolheiser.com/sip/sdk"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var Release = cli.Command{
	Name:    "releases",
	Aliases: []string{"release"},
	Usage:   "Commands for interacting with releases",
	Action:  doRelease,
	Subcommands: []*cli.Command{
		&ReleaseCreate,
		&ReleaseAttach,
	},
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  "csv",
			Usage: "Output results to a CSV file at `PATH`",
		},
	},
}

func doRelease(ctx *cli.Context) error {
	client, err := getClient(false)
	if err != nil {
		return err
	}

	owner, repo, err := askOwnerRepo()
	if err != nil {
		return err
	}

	releases, err := sdk.GetReleases(client, owner, repo, gitea.ListReleasesOptions{})
	if err != nil {
		return err
	}

	if ctx.String("csv") != "" {
		fi, err := os.Create(ctx.String("csv"))
		if err != nil {
			return err
		}
		if _, err := fi.WriteString(csv.Releases(releases)); err != nil {
			return err
		}
		fmt.Println(color.FgCyan.Format("Releases were exported to"), color.Info.Format(ctx.String("csv")))
		return fi.Close()
	}

	releaseMap := make(map[string]*gitea.Release)
	releaseList := make([]string, len(releases))
	for idx, release := range releases {
		key := fmt.Sprintf("%s (%s)", release.Title, release.TagName)
		releaseMap[key] = release
		releaseList[idx] = key
	}

	sel := &survey.Select{Options: releaseList, Message: "Releases"}
	var selection string
	if err := survey.AskOne(sel, &selection); err != nil {
		return err
	}

	note, err := markdown.Render(releaseMap[selection].Note)
	if err != nil {
		return err
	}

	fmt.Println(note)
	fmt.Printf("Release Date: %s\n", releaseMap[selection].PublishedAt.Format(csv.TimeFormat))
	return nil
}
