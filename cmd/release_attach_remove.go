package cmd

import (
	"fmt"
	"strconv"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/sdk"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var ReleaseAttachRemove = cli.Command{
	Name:    "remove",
	Aliases: []string{"delete", "rm"},
	Usage:   "Remove attachments from a release",
	Action:  doReleaseAttachRemove,
}

func doReleaseAttachRemove(_ *cli.Context) error {
	client, err := getClient(true)
	if err != nil {
		return err
	}

	releases, err := sdk.GetReleases(client, flag.Owner, flag.Repo, gitea.ListReleasesOptions{})
	if err != nil {
		return err
	}

	releaseNames := make([]string, len(releases))
	releaseMap := make(map[string]*gitea.Release)
	for idx, rel := range releases {
		releaseNames[idx] = rel.TagName
		releaseMap[rel.TagName] = rel
	}

	releaseQ := &survey.Select{Message: "Release", Options: releaseNames}
	var releaseA string
	if err := survey.AskOne(releaseQ, &releaseA); err != nil {
		return err
	}

	release := releaseMap[releaseA]
	attachments, err := sdk.GetReleaseAttachments(client, flag.Owner, flag.Repo, release.ID, gitea.ListReleaseAttachmentsOptions{})
	if err != nil {
		return err
	}

	attachmentMap := make(map[string]*gitea.Attachment)
	attachmentOptions := make([]string, len(attachments))
	for idx, attachment := range attachments {
		display := fmt.Sprintf("%s (%s)", attachment.Name, attachment.UUID)
		attachmentOptions[idx] = display
		attachmentMap[display] = attachment
	}

	attachmentQ := &survey.MultiSelect{Message: "Delete Attachments", Options: attachmentOptions}
	var attachmentA []string
	if err := survey.AskOne(attachmentQ, &attachmentA); err != nil {
		return err
	}

	for _, a := range attachmentA {
		attachment := attachmentMap[a]
		if _, err := client.DeleteReleaseAttachment(flag.Owner, flag.Repo, release.ID, attachment.ID); err != nil {
			return err
		}
	}

	info := color.Info
	cyan := color.New(color.FgCyan)
	fmt.Println(info.Format("Removed"), cyan.Format(strconv.Itoa(len(attachmentA))), info.Format("attachments."))
	fmt.Println(cyan.Format(fmt.Sprintf("%s/releases/tag/%s", flag.FullURL(), release.TagName)))
	return nil
}
