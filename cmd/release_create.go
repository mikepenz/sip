package cmd

import (
	"fmt"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/git"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var ReleaseCreate = cli.Command{
	Name:    "create",
	Aliases: []string{"new"},
	Usage:   "Create a new release",
	Action:  doReleaseCreate,
}

func doReleaseCreate(_ *cli.Context) error {
	client, err := getClient(true)
	if err != nil {
		return err
	}

	questions := []*survey.Question{
		{
			Name:     "tag",
			Prompt:   &survey.Input{Message: "Tag Name"},
			Validate: survey.Required,
		},
		{
			Name:     "target",
			Prompt:   &survey.Input{Message: "Target", Default: git.Branch(), Help: "Target ref, branch name or commit SHA"},
			Validate: survey.Required,
		},
		{
			Name:     "title",
			Prompt:   &survey.Input{Message: "Title"},
			Validate: survey.Required,
		},
		{
			Name:   "note",
			Prompt: &survey.Multiline{Message: "Notes"},
		},
		{
			Name:     "draft",
			Prompt:   &survey.Confirm{Message: "Draft", Default: false},
			Validate: survey.Required,
		},
		{
			Name:     "pre",
			Prompt:   &survey.Confirm{Message: "Pre-Release", Default: false},
			Validate: survey.Required,
		},
		{
			Name:   "attachments",
			Prompt: &survey.Multiline{Message: "Attachments", Help: "Enter file globs, each new line being a separate glob"},
		},
	}
	answers := struct {
		Tag         string
		Target      string
		Title       string
		Note        string
		Draft       bool
		Pre         bool
		Attachments string
	}{}

	if err := survey.Ask(questions, &answers); err != nil {
		return err
	}

	release, _, err := client.CreateRelease(flag.Owner, flag.Repo, gitea.CreateReleaseOption{
		TagName:      answers.Tag,
		Target:       answers.Target,
		Title:        answers.Title,
		Note:         answers.Note,
		IsDraft:      answers.Draft,
		IsPrerelease: answers.Pre,
	})
	if err != nil {
		return err
	}

	if answers.Attachments != "" {
		files, err := fileGlobs(answers.Attachments)
		if err != nil {
			return err
		}
		if err := attachFiles(client, release.ID, files); err != nil {
			return err
		}
	}

	info := color.Info
	cyan := color.New(color.FgCyan)
	fmt.Println(info.Format("Release"), cyan.Format(release.TagName), info.Format("created!"))
	fmt.Println(cyan.Format(fmt.Sprintf("%s/releases/tag/%s", flag.FullURL(), release.TagName)))
	return nil
}
