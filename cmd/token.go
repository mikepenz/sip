package cmd

import (
	"go.jolheiser.com/sip/config"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

var Tokens = cli.Command{
	Name:    "tokens",
	Aliases: []string{"token"},
	Usage:   "Manage access tokens",
	Action:  doTokenList,
	Subcommands: []*cli.Command{
		&TokensAdd,
		&TokensRemove,
	},
}

func doTokenList(_ *cli.Context) error {
	if len(config.Tokens) == 0 {
		beaver.Errorf("No tokens found! Add one with %s", color.FgMagenta.Format("sip token create"))
	}
	for idx, token := range config.Tokens {
		beaver.Infof("%d. %s (%s)", idx+1, token.Name, token.URL)
	}
	return nil
}
