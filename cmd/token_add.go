package cmd

import (
	"errors"

	"go.jolheiser.com/sip/config"
	"go.jolheiser.com/sip/flag"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var TokensAdd = cli.Command{
	Name:    "add",
	Aliases: []string{"create"},
	Usage:   "Add a new access token",
	Action:  doTokenAdd,
}

func doTokenAdd(ctx *cli.Context) error {
	token := ctx.Args().First()
	questions := []*survey.Question{
		{
			Name:     "name",
			Prompt:   &survey.Input{Message: "Local nickname for this token", Default: "gitea"},
			Validate: validateTokenName,
		},
		{
			Name:     "url",
			Prompt:   &survey.Input{Message: "URL for the Gitea instance", Default: flag.URL},
			Validate: survey.Required,
		},
	}
	answers := struct {
		Name string
		URL  string
	}{}

	if err := survey.Ask(questions, &answers); err != nil {
		return err
	}

	if token == "" {
		giteaQuestions := []*survey.Question{

			{
				Name:     "token",
				Prompt:   &survey.Input{Message: "Name for this token in Gitea", Default: "sip"},
				Validate: survey.Required,
			},
			{
				Name:     "username",
				Prompt:   &survey.Input{Message: "Username for the Gitea instance"},
				Validate: survey.Required,
			},
			{
				Name:     "password",
				Prompt:   &survey.Password{Message: "Password for the Gitea instance"},
				Validate: survey.Required,
			},
			{
				Name:   "otp",
				Prompt: &survey.Password{Message: "Two-Factor Token (if applicable)"},
			},
		}
		giteaAnswers := struct {
			Token    string
			Username string
			Password string
			OTP      string
		}{}

		if err := survey.Ask(giteaQuestions, &giteaAnswers); err != nil {
			return err
		}

		client, err := gitea.NewClient(answers.URL, gitea.SetBasicAuth(giteaAnswers.Username, giteaAnswers.Password))
		if err != nil {
			return err
		}
		if giteaAnswers.OTP != "" {
			client.SetOTP(giteaAnswers.OTP)
		}

		t, _, err := client.CreateAccessToken(gitea.CreateAccessTokenOption{Name: giteaAnswers.Token})
		if err != nil {
			return err
		}
		token = t.Token
	}

	config.Tokens = append(config.Tokens, config.Token{
		Name:  answers.Name,
		URL:   answers.URL,
		Token: token,
	})

	if err := config.Save(); err != nil {
		return err
	}

	beaver.Infof("Token saved! You can refer to it by using `--token %s` with commands!", answers.Name)

	return nil
}

func validateTokenName(ans interface{}) error {
	name := ans.(string)
	for _, token := range config.Tokens {
		if name == token.Name {
			return errors.New("token already exists")
		}
	}
	return survey.Required(ans)
}
