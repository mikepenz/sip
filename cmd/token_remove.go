package cmd

import (
	"fmt"

	"go.jolheiser.com/sip/config"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var TokensRemove = cli.Command{
	Name:    "remove",
	Aliases: []string{"delete"},
	Usage:   "Remove access tokens",
	Action:  doTokenRemove,
}

func doTokenRemove(_ *cli.Context) error {
	opts := make([]string, len(config.Tokens))
	for idx, token := range config.Tokens {
		opts[idx] = fmt.Sprintf("%s (%s)", token.Name, token.URL)
	}
	question := &survey.MultiSelect{
		Message:  "Which would you like to remove?",
		Options:  opts,
		PageSize: 10,
	}
	answers := make([]string, 0)

	if err := survey.AskOne(question, &answers); err != nil {
		return err
	}

	idxs := make([]int, len(answers))
	for idx, answer := range answers {
		for idy, token := range config.Tokens {
			if answer == fmt.Sprintf("%s (%s)", token.Name, token.URL) {
				idxs[len(answers)-idx-1] = idy
			}
		}
	}

	for _, idx := range idxs {
		config.Tokens = append(config.Tokens[:idx], config.Tokens[idx+1:]...)
	}

	if err := config.Save(); err != nil {
		return err
	}

	beaver.Infof("Removed %d token(s)! Remember to clean up in Gitea if necessary!", len(answers))

	return nil
}
