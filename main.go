package main

import (
	"os"

	"go.jolheiser.com/sip/cmd"
	"go.jolheiser.com/sip/config"

	"go.jolheiser.com/beaver"
)

var Version = "develop"

func main() {
	app := cmd.NewApp(Version)

	if err := config.Init(); err != nil {
		beaver.Fatal(err)
	}

	err := app.Run(os.Args)
	if err != nil {
		beaver.Fatal(err)
	}
}
