package qualify

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestParse(t *testing.T) {
	tt := []struct {
		Query      string
		Result     string
		Qualifiers []string
	}{
		{"test", "query->test", nil},
		{`"test"`, "query->test", nil},
		{"author:jolheiser", "author->jolheiser", nil},
		{`"this is a test" author:"jolheiser"`, "author->jolheiser,query->this is a test", nil},
		{`"this is" "a test" query`, "query->this is|a test|query", nil},
		{`label:"a bug" label:"a" label:"bug"`, "label->a bug|a|bug", nil},
		{`author:"jolheiser""`, `author->jolheiser"`, nil},
		{`author:""jolheiser""`, `author->"jolheiser"`, nil},
		{"test:testing", "query->test:testing", []string{"label"}},
	}

	for idx, tc := range tt {
		t.Run(strconv.Itoa(idx+1), func(t *testing.T) {
			parsed := result(Parse(tc.Query, tc.Qualifiers...))
			if parsed != tc.Result {
				t.Logf("\nwant: %s\n got: %s", tc.Result, parsed)
				t.Fail()
			}
		})
	}
}

func result(parsed Query) string {
	res := make([]string, 0, len(parsed))
	for key, values := range parsed {
		res = append(res, fmt.Sprintf("%s->%s", key, strings.Join(values, "|")))
	}
	sort.Strings(res)
	return strings.Join(res, ",")
}
