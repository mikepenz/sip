package sdk

import (
	"strings"

	"go.jolheiser.com/sip/qualify"

	"code.gitea.io/sdk/gitea"
)

// GetIssues returns all matching Issues from a Gitea instance
func GetIssues(client *gitea.Client, owner, repo string, opts gitea.ListIssueOption) ([]*gitea.Issue, error) {
	issues := make([]*gitea.Issue, 0)
	p := 1
	for {
		opts.Page = p
		list, _, err := client.ListRepoIssues(owner, repo, opts)
		if err != nil {
			return issues, err
		}
		p++
		issues = append(issues, list...)

		if len(list) == 0 {
			break
		}
	}
	return issues, nil
}

type State int

const (
	None State = iota
	Open
	Closed
	Merged
)

type IssueFilter struct {
	Query     string
	State     State
	Author    string
	Labels    []string
	Milestone string
}

//nolint:gocognit
// This function is high in cognitive complexity,
// however it is hopefully structured to be less painful than gocognit thinks
func (f *IssueFilter) Match(issue *gitea.Issue) bool {

	// Filter out state
	if f.State == Open && issue.State != gitea.StateOpen {
		return false
	}
	if f.State == Closed && issue.State != gitea.StateClosed {
		return false
	}
	if f.State == Merged && (issue.PullRequest == nil || !issue.PullRequest.HasMerged) {
		return false
	}

	// Filter out author
	if f.Author != "" && (issue.Poster == nil || !strings.EqualFold(f.Author, issue.Poster.UserName)) {
		return false
	}

	// Filter out labels
	for _, fl := range f.Labels {
		hasLabel := false
		for _, il := range issue.Labels {
			if fl == strings.ToLower(il.Name) {
				hasLabel = true
				break
			}
		}
		if !hasLabel {
			return false
		}
	}

	// Filter out milestone
	if f.Milestone != "" && (issue.Milestone == nil || !strings.EqualFold(f.Milestone, issue.Milestone.Title)) {
		return false
	}

	return true
}

func NewIssueFilter(query string) *IssueFilter {
	filter := &IssueFilter{}
	filters := qualify.Parse(query, "is", "author", "label", "milestone")

	if state, ok := filters["is"]; ok {
		switch state.Last() {
		case "open", "opened":
			filter.State = Open
		case "close", "closed":
			filter.State = Closed
		case "merge", "merged":
			filter.State = Merged
		default:
			filter.State = None
		}
	}

	if author, ok := filters["author"]; ok {
		filter.Author = author.Last()
	}

	if label, ok := filters["label"]; ok {
		filter.Labels = append(filter.Labels, label...)
	}

	if milestone, ok := filters["milestone"]; ok {
		filter.Milestone = milestone.Last()
	}

	filter.Query = filters.Query()

	return filter
}
